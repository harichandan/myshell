#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>

int list_files(int argc,  char *argv[]) {
    DIR *dir;
    struct dirent *dirp;
    struct stat buf;
    int index, opt;

    opterr = 0;

    if ((dir = opendir(".")) == NULL) {
        printf("ERROR: %s\n", strerror(errno));
        return -1;
    }

    if (argc == 1) {
        while((dirp = readdir(dir)) != NULL) {
            if(!strncmp(dirp->d_name, ".", 1) || !strncmp(dirp->d_name, "..", 2)) {
                continue;
            }
            printf("%s  ", dirp->d_name);
        }
        printf("\n");
        return 0;
    }

    while ((opt = getopt(argc, argv, "al")) != -1) {
        switch(opt) {
            case 'a':
                while((dirp = readdir(dir)) != NULL) {
                    if(!strncmp(dirp->d_name, ".", 1) || !strncmp(dirp->d_name, "..", 2)) {
                        continue;
                    }
                    printf("%s  ", dirp->d_name);
                }
                break;

            case 'l':
                while((dirp = readdir(dir)) != NULL) {
                    if(!strncmp(dirp->d_name, ".", 1) || !strncmp(dirp->d_name, "..", 2)) {
                        continue;
                    }
                    if(!stat(dirp->d_name, &buf)) {
                        printf("%6llu %6u %s\n", dirp->d_ino, buf.st_mode, dirp->d_name);
                    }
                    else {
                        printf("%s\n", strerror(errno));
                        break;
                    }
                }
                break;

            case '?':
                if (optopt)
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                return 1;

            default:
                abort();

        }
    }

    closedir(dir);

    for (index = optind; index < argc; index++)
        printf ("Non-option argument %s\n", argv[index]);
    free(argv);
    return 0;
}
