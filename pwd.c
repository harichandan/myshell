#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

void print_work_dir() {
  long size = pathconf(".", _PC_PATH_MAX);
  char* path;
  char* ptr;

  if((path = malloc(size)) == NULL) {
    printf("Error in allocating memory\n");
    return;
  }

  ptr = getcwd(path, (size_t) size);

  if (ptr == NULL) {
    printf("%s\n", strerror(errno));
    return;
  }

  printf("%s\n", path);
  free(path);
}
