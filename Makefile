CC = gcc
CFLAGS = -Wall -lc -g
TARGET = harsh

$(TARGET): shell.o parser.o pwd.o ls.o
	$(CC) $(CFALGS) -o $(TARGET) shell.o parser.o pwd.o ls.o
	rm -rf *.o

shell.o: shell.c parser.h
	gcc -c shell.c

parser.o: parser.c parser.h
	gcc -c parser.c

pwd.o: pwd.c pwd.h
	gcc -c pwd.c

ls.o: ls.c ls.h
	gcc -c ls.c

clean: 
	rm -rf $(TARGET)
