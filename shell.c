#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "parser.h"

#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_BLUE    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int main () {
    char input[100];
    char user[20];
    fflush(NULL);
    while (1) {
        strncpy(user, getlogin(), 20);
        printf (ANSI_COLOR_GREEN "%s@" ANSI_COLOR_BLUE "harsh$ " ANSI_COLOR_RESET, user);
        fgets (input, 100, stdin);
        if (!strncmp(input, "exit", 4))
            exit(0);
        parse (input);
    }
    return 0;
}
