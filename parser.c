/*
 * A simple parser
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pwd.h"
#include "ls.h"

void parse(char* input)
{
    int argc=0, n, i=0, j=0;
    char **argv;

    argv = malloc(10 * sizeof(char*));

    for(int i=0; i<10; i++)
        argv[i] = malloc(20 * sizeof(char));


    for (i=0; i<strlen(input); i++) {
        if (input[i] != ' ' && input[i]!='\0') {
            argv[argc][j++] = input[i];
        }
        else {
            argv[argc][j] = '\0';
            argc++;
            j=0;
        }
    }
    argv[argc++][j] = '\0';

    if(!strncmp(argv[0], "pwd", 3))
        print_work_dir();
    else if(!strncmp(argv[0], "ls", 2))
        list_files(argc, argv);
    else
        printf("Unsupported operation: %s", argv[0]);
}
